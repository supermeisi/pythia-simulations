#include <iostream>
#include <vector>
#include <thread>

#include "TFile.h"
#include "TTree.h"
#include "TApplication.h"
#include "TThread.h"

#include "Pythia8/Pythia.h"

int run = 0;
int n_runs = 1;
int n_events = 10;
int n_threads = 4;

void *handle(void *ptr)
{
    int ith = (long)ptr;

    bool verbose = false;

    std::stringstream filename;
    filename << "output" << run << "_t" << ith << ".root";

    int iseed = ith + run*n_threads;

    std::stringstream seed;
    seed << "Random:seed = " << iseed;
    
    std::cout << "Seed: " << iseed << std::endl;

    TFile output(filename.str().c_str(), "recreate");
    
    TTree header("header", "header");
    header.Branch("seed", &iseed, "iseed/I");
    header.Branch("events", &n_events, "n_events/I");
    header.Fill();

    TTree tree("tree", "tree");

    int id, mother1, mother2, event, entries, parnum, daughter1, daughter2;
    double m, px, py, pz;

    tree.Branch("event", &event, "event/I");
    tree.Branch("entries", &entries, "entries/I");
    tree.Branch("parnum", &parnum, "parnum/I");
    tree.Branch("mother1", &mother1, "mother1/I");
    tree.Branch("mother2", &mother2, "mother2/I");
    tree.Branch("daughter1", &daughter1, "daughter1/I");
    tree.Branch("daughter2", &daughter2, "daughter2/I");
    tree.Branch("id", &id, "id/I");
    tree.Branch("m", &m, "m/D");
    tree.Branch("px", &px, "px/D");
    tree.Branch("py", &py, "py/D");
    tree.Branch("pz", &pz, "pz/D");

    Pythia8::Pythia pythia;

    pythia.readString("Beams:idA = 11");
    pythia.readString("Beams:idB = -11");
    pythia.readString("Beams:eCM = 5");
    /*pythia.readString("HardQCD:all = on");
    pythia.readString("PromptPhoton:all = on");
    pythia.readString("WeakZ0:gmZmode = 0");
    pythia.readString("WeakDoubleBoson:all = on");
    pythia.readString("WeakBosonAndParton:all = on");
    pythia.readString("PhotonCollision:all = on");
    pythia.readString("PhotonParton:all = on");
    pythia.readString("Onia:all = on");*/
    pythia.readString("Random:setSeed = on");
    pythia.readString("WeakSingleBoson:ffbar2ffbar(s:gmZ) = on");
  
    /*Pythia8::ParticleDataEntryPtr Z0 = pythia.particleData.findParticle(23);
    Z0->clearChannels();
    Z0->addChannel(1, 0.1539840, 0, 4, -4);
    Z0->addChannel(1, 0.1539840, 0, 5, -5);*/
  
    pythia.readString(seed.str());

    pythia.init();

    for(int i = 0; i < n_events; i++)
    {
        if(!pythia.next()) continue;

        entries = pythia.event.size();

        event = i;

        //pythia.event.list();


        if(verbose)
        {
            std::cout << "Event: " << i << std::endl;
            std::cout << "Event size: " << entries << std::endl;
        }
        
        bool accept = false;
        
        for(int j = 0; j < entries; j++)
        {
            if(abs(pythia.event[j].id()) == 411)
            {
                accept = true;
                break;
            }
        }
        
        if(!accept) continue;

        for(int j = 0; j < entries; j++)
        {
            id = pythia.event[j].id();
            
            parnum = j;

            mother1 = pythia.event[parnum].mother1();
            mother2 = pythia.event[parnum].mother2();

            daughter1 = pythia.event[parnum].daughter1();
            daughter2 = pythia.event[parnum].daughter2();

            m = pythia.event[parnum].m();

            px = pythia.event[parnum].px();
            py = pythia.event[parnum].py();
            pz = pythia.event[parnum].pz();

            if(verbose) std::cout << j << " " << id << " " << m << " " << mother1 << " " << mother2 << std::endl;


            //Hans Debsinski

            //tree.Fill();

            if(abs(id) == 411)
            {
                std::vector<int> daughters = pythia.event.daughterList(j);

                for(int k = 0; k < daughters.size(); k++)
                {
                    id = pythia.event[daughters[k]].id();

                    parnum = daughters[k];

                    if(abs(id) == 211 || abs(id) == 13)
                    {
                        mother1 = pythia.event[parnum].mother1();
                        mother2 = pythia.event[parnum].mother2();

                        daughter1 = pythia.event[parnum].daughter1();
                        daughter2 = pythia.event[parnum].daughter2();

                        m = pythia.event[parnum].m();

                        px = pythia.event[parnum].px();
                        py = pythia.event[parnum].py();
                        pz = pythia.event[parnum].pz();

                        if(verbose) std::cout << j << " " << id << " " << m << " " << mother1 << " " << mother2 << std::endl;

                        tree.Fill();
                    }
                }
            }  
        }
    }

    output.Write();
    output.Close();

    return 0;
}

int main(int argc, char** argv)
{
    TThread *th[16];
  
    for(int i = 0; i < argc; i++)
    {
        if(strcmp(argv[i],"-s") == 0) run = std::atoi(argv[i+1]);
        if(strcmp(argv[i],"-r") == 0) n_runs = std::atoi(argv[i+1]);
        if(strcmp(argv[i],"-n") == 0) n_events = std::atoi(argv[i+1]);
        if(strcmp(argv[i],"-t") == 0) n_threads = std::atoi(argv[i+1]);
    }

    for(int i = 0; i < n_runs; i++)
    {
        run++;

        for(int j = 0; j < n_threads; j++)
        {
            th[j] = new TThread("th", handle, (void*)j);

            th[j]->Run();
        }

        for(int j = 0; j < n_threads; j++)
        {
            th[j]->Join();
        }
    }

    return 0;
}

